from parser_table import ParserTable
from stack import Stack
from tag import TAG


class Parser(ParserTable):

    def __init__(self, lexer):
        super().__init__()
        self.resultado_parser = ''
        self.top = None
        self.stack = Stack()
        self.synch = False
        self.skip = False
        self.qtd_errors = 0
        self.lexer = lexer
        self.token = lexer.prox_token()

    def init_parser(self):
        self.push('Classe')

        top = ''

        while True:
            if not self.skip:
                top = self.pop()

            self.get_token_by_rule(top, self.token)

            if self.is_non_terminal(top):
                rule = self.get_rule(top, self.token.get_lexeme())
                self.push_rule(rule)
            elif self.is_terminal(top):
                if not top == self.token.get_lexeme():
                    self.resultado_parser += f'Erro sintatico (Skip): Token inesperado {self.token.get_lexeme()}\n'
                    self.get_next_token()
                    self.skip = True
                else:
                    self.skip = False
                    print(f'Matching terminal: {self.token.get_lexeme()}')
                    self.resultado_parser += f'Matching terminal: {self.token.get_lexeme()}\n'
                    self.get_next_token()

            if self.token.get_lexeme() == TAG.EOF.name:
                self.resultado_parser += f'Matching terminal: {self.token.get_lexeme()}\n'
                print('Codigo compilado com sucesso')
                break

            if self.qtd_errors == 5:
                break

    @staticmethod
    def get_token_by_rule(top, token):

        if token.get_class() == TAG.ID.name:
            token.set_lexeme('ID')
        elif token.get_class() == TAG.STRING.name:
            token.set_lexeme('ConstString')
        elif token.get_class() == TAG.INT.name:
            token.set_lexeme('ConstInt')
        elif token.get_class() == TAG.FLOAT.name:
            token.set_lexeme('ConstFloat')
        elif token.get_class() == 'ConstString':
            token.set_lexeme('ConstString')
        else:
            return

    def push_rule(self, regra):
        split_rule = regra.split()
        self.resultado_parser += f'{regra} \n'
        for i in range(len(split_rule), 0, -1):
            regra_at = split_rule[i - 1]
            self.push(regra_at)

    def is_terminal(self, s):
        for terminal in self.terminals:
            if terminal == s:
                return True
        return False

    def is_non_terminal(self, s):
        for no_terminal in self.no_terminals:
            if no_terminal == s:
                return True
        return False

    def get_next_token(self):
        self.token = self.lexer.prox_token()
        token_lexeme = self.token.get_lexeme()
        return token_lexeme

    def push(self, s):
        self.stack.push(s)

    def pop(self):
        return self.stack.pop()

    def show_error(self, message):
        self.resultado_parser += f'{message} \n'
        print(self.resultado_parser)

    def get_rule(self, no_terminal, terminal):
        row = self.get_non_terminal_index(no_terminal)
        column = self.get_terminal_index(terminal)

        rule = self.table[row][column]

        if rule == 'synch':
            self.show_error(f'Erro sintatico (Sync): Token Inesperado {self.token.get_lexeme()}\n')
            self.qtd_errors += 1
            top = self.pop()
            rule_aux = self.get_rule(top, self.token.get_lexeme())
            self.push_rule(rule_aux)
            self.skip = False
        elif rule == 'skip':
            self.show_error(f'Erro sintatico (Sync): Token Inesperado {self.token.get_lexeme()}\n')
            self.qtd_errors += 1
            self.get_next_token()
            self.skip = True
        else:
            self.skip = False
        return rule

    def get_non_terminal_index(self, non_terminal):
        for i in range(len(self.no_terminals)):
            if non_terminal == self.no_terminals[i]:
                return i
        self.show_error(f'{non_terminal} nao e um nonterminal')
        return -1

    def get_terminal_index(self, terminal):
        for i in range(len(self.terminals)):
            if terminal == self.terminals[i]:
                return i
        self.show_error(f'{terminal} nao e um terminal')
        return -1
