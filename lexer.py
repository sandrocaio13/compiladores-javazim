#encoding: utf-8
import sys
import re
from token_code import Token
from ts import Ts
from tag import TAG

class Lexer():

    def __init__(self, path_file):
        try:
            self.file_code = open(path_file, 'rb')
            self.reading = 1
            self.n_linha = 1
            self.n_column = 0
            self.error = False
            self.n_errors = 0
            self.ts = Ts()
        except IOError:
            print('Não foi possível ler o arquivo')
            sys.exit(0)

    def back_pointer(self, n_back=1):
        if self.reading != TAG.EOF.value:
            self.file_code.seek(self.file_code.tell() - n_back)

    def prox_token(self):
        lexeme = ''
        step = 1
        while(True):
            char = '\u0000'
            ant = '\u0000'
            self.reading = self.file_code.read(1)
            char = self.reading.decode('ascii')
            while(True):
                if not char == '':
                    if step == 1:
                        self.n_column += 1
                        if char == ' ' or char == '\t' or char == '\r':
                            step = 1
                        elif char == '\n':
                            step = 1
                            self.n_column = 1
                            self.n_linha += 1
                        elif char == '+':
                            # step 2
                            return Token(TAG.PLU_SIGNAL.name, '*', self.n_column, self.n_linha)
                        elif char == '-':
                            lexeme += char
                            step = 3
                        elif char == '*':
                            # step 4
                            return Token(TAG.MUL_SIGNAL.name, '*', self.n_column, self.n_linha)
                        elif char == '/':
                            step = 5
                        elif char.isalpha():
                            step = 6
                            lexeme += char
                        elif char.isnumeric():
                            step = 8
                            lexeme += char
                        elif char == '"':
                            lexeme += char
                            step = 13
                        elif char == '&':
                            step = 16
                            lexeme += char
                        elif char == ';':
                            # step 24
                            return Token(TAG.SC_SIGNAL.name, ';', self.n_column, self.n_linha)
                        elif char == '|':
                            step = 25
                        elif char == '(':
                            # step 27
                            return Token(TAG.OP_SIGNAL.name, '(', self.n_column, self.n_linha)
                        elif char == ')':
                            # step 28
                            return Token(TAG.CP_SIGNAL.name, ')',self.n_column, self.n_linha)
                        elif char == '{':
                            # step 29
                            return Token(TAG.OB_SIGNAL.name, '{', self.n_column, self.n_linha)
                        elif char == '}':
                            # step 30
                            return Token(TAG.CB_SIGNAL.name, '}', self.n_column, self.n_linha)
                        elif char == ',':
                            # step 31
                            return Token(TAG.COM_SIGNAL.name, ',', self.n_column, self.n_linha)
                        elif char == '!':
                            step = 32
                        elif char == '=':
                            step = 35
                        elif char == '>':
                            step = 37
                        elif char == '<':
                            step = 40
                        else:
                            step = 1
                            self.panic_mode(char)
                        break
                    if step == 3:
                        if char == "-" or char == '(':
                            step = 1
                            self.back_pointer()
                            return Token('NEG_SIGNAL', '-', self.n_column, self.n_linha)
                        else:
                            step = 1
                            self.back_pointer()
                            return Token(TAG.MIN_SIGNAL.name, '-', self.n_column, self.n_linha)
                        break
                    if step == 5:
                        if char == '/':
                            step = 1
                            self.n_linha += 1
                        if char == '*':
                            step = 1
                            teste = '\u0000'
                            while('*/' not in teste):
                                # step 22
                                # step 23
                                self.n_linha += 1
                                teste = self.file_code.readline().decode('ascii')
                        elif char == '/':
                            teste = self.file_code.readline().decode('ascii')
                        else:
                            # step 5
                            self.back_pointer()
                            return Token(TAG.DS_SIGNAL.name, '/', self.n_column, self.n_linha)
                        break
                    if step == 6:
                        if char.isalnum():
                            lexeme += char
                        elif char == ' ':
                            self.back_pointer()
                            step = 7
                        else:
                            aux_char = self.file_code.read(1).decode('ascii')
                            self.back_pointer()
                            if not aux_char.isalnum():
                                self.back_pointer()
                                step = 7
                            else:
                                self.panic_mode(char)
                        break
                    if step == 7:
                        self.back_pointer()
                        token = self.ts.getToken(lexeme)
                        if token is None:
                            return Token(TAG.ID.value, lexeme, self.n_column, self.n_linha)
                        token.set_line(self.n_linha)
                        token.set_column(self.n_column)
                        return token
                    if step == 8:
                        if char.isnumeric():
                            lexeme += char
                            ant = char
                        elif char == '.':
                            step = 10
                            lexeme += char
                        else:
                            self.back_pointer()
                            return Token(TAG.INT.value, lexeme, self.n_column, self.n_linha)
                        break
                    if step == 10 or step == 11:
                        if char.isnumeric():
                            lexeme += char
                        else:
                            # step 12
                            return Token(TAG.REAL.value, lexeme, self.n_column, self.n_linha)
                        break
                    if step == 13:
                        if char.isalnum() or char == '"' or char == ' ':
                            lexeme += char
                        elif lexeme != '""' and lexeme[len(lexeme) - 1] == '"':
                            #step 15
                            self.back_pointer()
                            return Token(TAG.STRING.name, lexeme, self.n_column, self.n_linha)
                        else:
                            print('error')
                            sys.exit(0)
                        break
                    if step == 9:
                        if char.isnumeric():
                            lexeme += char
                        elif lexeme[len(lexeme) - 1] == '.':
                            print('erro')
                            sys.exit(0)
                            break
                        else:
                            self.back_pointer()
                            return Token(TAG.FLOAT.name, lexeme, self.n_column, self.n_linha)
                        break
                    if step == 16:
                        if char == '&':
                            self.back_pointer()
                            return Token(TAG.AND_SIGNAL.name, '&&', self.n_column, self.n_linha)
                        elif True: #Arrumar depois
                            return None
                        break
                    if step == 25:
                        if char == '|':
                            self.back_pointer()
                            return Token(TAG.OR_SIGNAL.name, '||', self.n_column, self.n_linha)
                        elif True: #Arruma depois
                            return None
                    if step == 32:
                        if char == '=':
                            #step 33
                            return Token(TAG.DIF_SIGNAL.name, '!=', self.n_column, self.n_linha)
                        else:
                            #step 34
                            self.back_pointer()
                            return Token(TAG.EXC_SIGNAL.name, '!', self.n_column, self.n_linha)
                        break
                    if step == 35:
                        if char == '=':
                            #step 35
                            return Token(TAG.EQ_SIGNAL.name, '==', self.n_column, self.n_linha)
                        else:
                            #step 36
                            self.back_pointer()
                            return Token(TAG.ATR_SIGNAL.name, '=', self.n_column, self.n_linha)
                        break
                    if step == 37:
                        if char == '=':
                            # step 38
                            return Token(TAG.LGE_SIGNAL.name, '>=', self.n_column, self.n_linha)
                        else:
                            # step 39
                            self.back_pointer()
                            return Token(TAG.LG_SIGNAL, '>', self.n_column, self.n_linha)
                        break
                    if step == 40:
                        if char == '=':
                            # step 41
                            return Token(TAG.LTE_SIGNAL.name, '<=', self.n_column, self.n_linha)
                        else:
                            # step 42
                            self.back_pointer()
                            return Token(TAG.LT_SIGNAL, '<', self.n_column, self.n_linha)
                        break
                else:
                    return Token(TAG.EOF.name, 'EOF', self.n_column, self.n_linha)

    def printToken(self):
        token = Token()
        print('\n------ Tokens -------')
        while(token != None and token.get_class() != TAG.EOF.name):
            if self.n_errors < 5:
                token = self.prox_token()
                if token != None:
                    self.ts.setTs(token.get_class(), token)
                    print(f'Token: {token.to_string()}')
            else:
                sys.exit(0)
    
    def printTs(self):
        if self.n_errors == 0:
            print('\n------ Tabela Simbolo -------')
            self.ts.getTs()
            print('Código compilado com sucesso')
        else:
            print(f'Seu código contém {self.n_errors} erro(s), por favor, corriga os seu erro(s) e compile novamente!')

    def panic_mode(self, char):
        self.n_errors += 1
        self.error = True
        print(f'Símbolo {char} desconhecido.')
        print(f'Erro na linha: {self.n_linha}, coluna: {self.n_column}, padrão desconheido.')
