from lexer import Lexer
from parser import Parser

if __name__ == "__main__":
    lexer = Lexer('teste.jz')
    parser = Parser(lexer)
    parser.init_parser()
