from enum import Enum


class TAG(Enum):
    # Fim de arquivo
    EOF = -1

    # Palavras Chvaes
    CLASS = 'CLASS'
    KW = 'KW'
    ID = 'ID'
    INT = 'INT'
    REAL = 'REAL'
    FLOAT = 'FLOAT'
    DOUBLE = 'DOUBLE'
    STRING = 'STRING'

    # Simbolos
    OP_SIGNAL = '('
    CP_SIGNAL = ')'
    SC_SIGNAL = ';'
    COM_SIGNAL = ','
    QUO_SIGNAL = '"'
    ATR_SIGNAL = '='
    OB_SIGNAL = '{'
    CB_SIGNAL = '}'

    # Operadores
    OR_SIGNAL = '||'
    AND_SIGNAL = '&'
    LG_SIGNAL = '>'
    LGE_SIGNAL = '>='
    LTE_SIGNAL = '<='
    LT_SIGNAL = '<'
    EQ_SIGNAL = '=='
    DIF_SIGNAL = '!='
    DS_SIGNAL = '/'
    MUL_SIGNAL = '*'
    MIN_SIGNAL = '-'
    PLU_SIGNAL = '+'
    EXC_SIGNAL = '!'
