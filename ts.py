from token_code import Token
from tag import TAG

class Ts:

    def __init__(self):
        self.tableSimbols = {}
        with open('keywords.jzb', 'r') as f:
            for keys in f.readlines():
                word = Token(TAG.KW.name, keys.split()[0])
                self.tableSimbols[keys.split()[0]] = word

    def setTs(self, name, token):
        self.tableSimbols[name] = token

    def getToken(self, lexeme):
        return self.tableSimbols.get(lexeme)

    def getTs(self):
        for i in sorted(self.tableSimbols):
            print(f'<{i}, {self.tableSimbols[i].print_token_ts()}>')
