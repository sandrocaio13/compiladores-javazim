class Token():

    def __init__(self, classe=None, lexeme=None, column=None, line=None):
        self.classe = classe
        self.lexeme = lexeme
        self.column = column
        self.line = line
    
    def set_class(self, classe):
        self.classe = classe
    
    def get_class(self):
        return self.classe
    
    def set_lexeme(self, lexeme):
        self.lexeme = lexeme
    
    def get_lexeme(self):
        return self.lexeme
    
    def set_column(self, column):
        self.column = column
    
    def set_line(self, line):
        self.line = line

    def to_string(self):
        return f'<"{self.classe}", "{self.lexeme}"> Linha: {self.line} Coluna: {self.column}'
    
    def print_token_ts(self):
        return f'<"{self.classe}", "{self.lexeme}">'
